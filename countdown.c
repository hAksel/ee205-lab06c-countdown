///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts how much time as passed from a random date
//
// Example:
//   reference date: Sat Feb 10 15:46:40 2001
//   Years: 20 Days: 169 Hours: 6 Minutes: 2 Seconds: 25
//
// @author  Aksel Sloan <aksel@hawaii.edu>
// @date    22 February 2022 
///////////////////////////////////////////////////////////////////////////////
#include <time.h> 
#include <string.h>
#include <stdio.h>
#include <unistd.h>
//#define DEBUG

int main() {
   int delta_years, delta_days, delta_hours, delta_minutes, delta_seconds; 
   time_t               referenceTime; 
   time_t               currentTime;
   time_t               timeDifference; 
   time_t               shiftedTimeDif; 

   time_t progression = -2208988799; 

   #ifdef DEBUG
   time_t seventies = 1;
   //Progression fixes the problems with struct starting counting at 1900 
   //while time_t = 0 is 1970 
   printf ("seventies = [%s]\n", ctime(&seventies)); 
   #endif
   
   referenceTime  =  1000000000;
   printf( "Reference time in HST: %s\n", ctime( &referenceTime ) ); //HERE
   int x = 0;
   while (x < 1 ){
      currentTime = time(0);
      timeDifference = currentTime - referenceTime;
      shiftedTimeDif = timeDifference + progression; 
      struct tm * difference = gmtime( &shiftedTimeDif );  
         delta_years =     difference-> tm_year;
         delta_days  =     difference-> tm_yday;
         delta_hours =     difference-> tm_hour;
         delta_minutes =   difference-> tm_min;
         delta_seconds =   difference-> tm_sec;
  
   #ifdef DEBUG
   printf ("reference time is    [%s]\n", ctime(&referenceTime)); 
   printf ("current time is      [%s]\n", ctime(&currentTime));
   printf ("actual subtraction   [%s]\n", ctime(&timeDifference));
   x++; 
   #endif 
      sleep(1);
      printf( "Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n" 
            ,delta_years, delta_days, delta_hours, delta_minutes, delta_seconds );
   }
   


   return 0;
}
